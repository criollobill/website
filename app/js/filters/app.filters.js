/*
* TO CREATE NEW FILTER, SEE THE MODELS IN THE LINK (https://github.com/the-darc/string-mask)
* OR FOLLOW THE TIPS BELLOW:
*
* 0 - Any numbers
* 9 - Any numbers (Optional)
* # - Any numbers (recursive)
* A - Any aphanumeric character Not implemented yet
* a - Any aphanumeric character (Optional) Not implemented yet
* S - Any letter
* U - Any letter (All lower case character will be mapped to uppercase)
* L - Any letter (All upper case character will be mapped to lowercase)
* $ - Escape character, used to escape any of the special formatting characters.
*
*/

angular.module('app.filters', [])
.filter('PhoneBR', filterPhoneBR)
.filter('CPF', filterCPF)
.filter('CNPJ', filterCNPJ)
.filter('ZipCodeBR', filterZipCodeBR)
.filter('dateUnix', filterDateUnix)
.filter('cutString', filterCutString)
.filter('removeExtension', filterRemoveExtension)
.filter('removeAccents', filterRemoveAccents);
