function filterCNPJ(){
    return (cnpj) => {
        const formatter = new StringMask('00.000.000/0000-00');
        return formatter.apply(cnpj);
    };
}